<?php
namespace Nilopc\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
            $builder->add('username');
            $builder->add('email', 'email');
            $builder->add('plainPassword', 'repeated', array('type' => 'password'));
            $builder->add('username','text',array( 'invalid_message' => 'register.form.empty.field'));
            $builder->add('date_birth', 'birthday');
            $builder->add('first_name','text',array( 'invalid_message' => 'register.form.empty.field'));
            $builder->add('last_name','text',array('invalid_message' => 'register.form.empty.field'));            
            $builder->add('address','text',array( 'invalid_message' => 'register.empty.field'));
            $builder->add('allowsnewsletters', 'checkbox', array('required' => false));
            
    }



    public function getName()
    {
        return 'nilopc_user_registration';
    }
}
