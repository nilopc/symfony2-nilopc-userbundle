<?php

namespace Nilopc\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NilopcUserBundle extends Bundle
{
	public function getParent()
    {
        return 'FOSUserBundle';
    }
}
