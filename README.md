Run the following:

	php app/console init:acl

Add the following to your security.yml file:


	security:
	    acl:
		connection: default

	    firewalls:
		# Firewall global utilizado en la parte pública o frontend
		frontend:
		    pattern:        ^/*
		    provider:       user_provider ## defined in this same file, under providers
		    anonymous:      ~
		    form_login:
		        login_path: frontend_user_login
		        check_path: frontend_user_login_check
		    logout:
		        path:       frontend_user_logout
		    remember_me:
		        key:        1234567890@Nilopc\UserBundle@1234567890
		        lifetime:   604800  # 604800 = 3600 * 24 * 7 = 1 week

	    access_control:
		- { path: ^/login,          roles: IS_AUTHENTICATED_ANONYMOUSLY }
		- { path: ^/register,       roles: IS_AUTHENTICATED_ANONYMOUSLY }
	       # - { path: ^/user/*,         roles: ROLE_USER                    }

	    encoders:
		Nilopc\UserBundle\Entity\Userfrontend:        { algorithm: sha512, iterations: 50 }
		Symfony\Component\Security\Core\User\User: sha512

	    providers:
		user_provider:  
		    entity: { class: Nilopc\UserBundle\Entity\Userfrontend, property: email }

	    role_hierarchy:
		ROLE_ADMIN: [ROLE_BUSINESS, ROLE_USER, ROLE_ALLOWED_TO_SWITCH]



Add the following to config.yml


	# FOSUserBundle <-> NilopcUserBundle 
	fos_user:
	    db_driver:                  orm # other valid values are 'mongodb', 'couchdb' and 'propel'
	    firewall_name:              main
	    user_class:                 Nilopc\UserBundle\Entity\User
	    use_listener:               true
	    use_username_form_type:     true

	    from_email:
		address:                %noreply_address%
		sender_name:            %noreply_name%

	    profile:
		form:
		    type:               fos_user_profile
		    handler:            fos_user.profile.form.handler.default
		    name:               fos_user_profile_form
		    validation_groups:  [Profile]

	    change_password:
		form:
		    type:               fos_user_change_password
		    handler:            fos_user.change_password.form.handler.default
		    name:               fos_user_change_password_form
		    validation_groups:  [ChangePassword]

	    registration:
		confirmation:
		    from_email: # Use this node only if you don't want the global email address for the confirmation email
		        address:        %noreply_address%
		        sender_name:    %noreply_name%
		    enabled:            true # change to true for required email confirmation
		    template:           NilopcUserBundle:Registration:email.txt.twig
		form:
		    type:               nilopc_user_registration
		    handler:            fos_user.registration.form.handler.default
		    name:               fos_user_registration_form
		    validation_groups:  [Registration]

	    resetting:
		token_ttl: 86400
		email:
		    from_email: # Use this node only if you don't want the global email address for the resetting email
		        address:        %noreply_address%
		        sender_name:    %noreply_name%
		    template:           NilopcUserBundle:Resetting:email.txt.twig
		form:
		    type:               fos_user_resetting
		    handler:            fos_user.resetting.form.handler.default
		    name:               fos_user_resetting_form
		    validation_groups:  [ResetPassword]

	    service:
		mailer:                 fos_user.mailer.default
		email_canonicalizer:    fos_user.util.canonicalizer.default
		username_canonicalizer: fos_user.util.canonicalizer.default
		user_manager:           fos_user.user_manager.default

	    template:
		engine:                 twig
		theme:                  NilopcUserBundle::form.html.twig

	    group:
		group_class:            Nilopc\UserBundle\Entity\Group
		group_manager:          fos_user.group_manager.default
		form:
		    type:               fos_user_group
		    handler:            fos_user.group.form.handler.default
		    name:               fos_user_group_form
		    validation_groups:  [Registration]

