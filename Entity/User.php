<?php

namespace Nilopc\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\ExecutionContext;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Entity\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="_users")
 */
class User extends BaseUser
{

    /** @ORM\OneToMany(targetEntity="Nilopc\CommentsBundle\Entity\BusinessComments", mappedBy="user_object", orphanRemoval=true, cascade={"persist","remove"}) */
    protected $gym_comments;    



  /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var string $first_name
     *
     * @ORM\Column(name="first_name", type="string", length=100)
     * @Assert\NotBlank()
     */
    private $first_name;

    /**
     * @var string $last_name
     *
     * @ORM\Column(name="last_name", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $last_name;


    /**
     * @var text $address
     *
     * @ORM\Column(name="address", type="text")
     * @Assert\NotBlank()
     */
    private $address;

    /**
     * @var boolean $allows_newsletters
     *
     * @ORM\Column(name="allows_newsletters", type="boolean")
     * @Assert\Type(type="bool")
     */
    private $allows_newsletters;

    /**
     * @var datetime $date_registered
     *
     * @ORM\Column(name="date_registered", type="datetime")
     * @Assert\DateTime()
     */
    private $date_registered;

    /**
     * @var datetime $date_birth
     *
     * @ORM\Column(name="date_birth", type="datetime")
     * @Assert\DateTime()
     */
    private $date_birth;



    /**
     * @ORM\ManyToMany(targetEntity="Nilopc\UserBundle\Entity\Group")
     * @ORM\JoinTable(name="_user_group_rel",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     */
    protected $groups;


    public function __construct()
    {
        $this->date_registered= new \DateTime();
        $this->user_roles = new ArrayCollection();
        $this->gym_comments = new ArrayCollection();
        parent::__construct();
    }


    /**
     * Returns an ARRAY of Role objects with the default Role object appended.
     * @return array
     */
    public function getUserRoles()
    {
        return array_merge( $this->user_roles->toArray(), array( new Role( parent::ROLE_DEFAULT ) ) );
    }
    
    /**
     * Returns the true ArrayCollection of Roles.
     * @return Doctrine\Common\Collections\ArrayCollection 
     */
    public function getUserRolesCollection()
    {
        return $this->user_roles;
    }
    
    /**
     * Pass a string, get the desired Role object or null.
     * @param string $role
     * @return Role|null
     */
    public function getUserRole( $role )
    {
        foreach ( $this->getUserRoles() as $roleItem )
        {
            if ( $role == $roleItem->getRole() )
            {
                return $roleItem;
            }
        }
        return null;
    }
    
    /**
     * Pass a string, checks if we have that Role. Same functionality as getRole() except returns a real boolean.
     * @param string $role
     * @return boolean
     */
    public function hasUserRole( $role )
    {
        if ( $this->getUserRole( $role ) )
        {
            return true;
        }
        return false;
    }

    /**
     * Adds a Role OBJECT to the ArrayCollection. Can't type hint due to interface so throws Exception.
     * @throws Exception
     * @param Role $role 
     */
    public function addUserRole( $role )
    {
        if ( !$role instanceof Role )
        {
            throw new \Exception( "addUserRole takes a Role object as the parameter" );
        }
        
        if ( !$this->hasUserRole( $role->getRole() ) )
        {
            $this->user_roles->add( $role );
        }
    }
    
    /**
     * Pass a string, remove the Role object from collection.
     * @param string $role 
     */
    public function removeUserRole( $role )
    {
        $roleElement = $this->getRole( $role );
        if ( $roleElement )
        {
            $this->user_roles->removeElement( $roleElement );
        }
    }
    
    /**
     * Pass an ARRAY of Role objects and will clear the collection and re-set it with new Roles.
     * Type hinted array due to interface.
     * @param array $roles Of Role objects.
     */
    public function setUserRoles( array $roles )
    {
        $this->user_roles->clear();
        foreach ( $roles as $role )
        {
            $this->addUserRole( $role );
        }
    }
    
    /**
     * Directly set the ArrayCollection of Roles. Type hinted as Collection which is the parent of (Array|Persistent)Collection.
     * @param Doctrine\Common\Collections\Collection $role 
     */
    public function setUserRolesCollection( Collection $collection )
    {
        $this->user_roles = $collection;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set first_name
     *
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;
    }

    /**
     * Get first_name
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;
    }

    /**
     * Get last_name
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set address
     *
     * @param text $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Get address
     *
     * @return text 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set allows_newsletters
     *
     * @param boolean $allowsNewsletters
     */
    public function setAllowsNewsletters($allowsNewsletters)
    {
        $this->allows_newsletters = $allowsNewsletters;
    }

    /**
     * Get allows_newsletters
     *
     * @return boolean 
     */
    public function getAllowsNewsletters()
    {
        return $this->allows_newsletters;
    }

    /**
     * Set date_registered
     *
     * @param datetime $dateRegistered
     */
    public function setDateRegistered($dateRegistered)
    {
        $this->date_registered = $dateRegistered;
    }

    /**
     * Get date_registered
     *
     * @return datetime 
     */
    public function getDateRegistered()
    {
        return $this->date_registered;
    }

    /**
     * Set date_birth
     *
     * @param datetime $dateBirth
     */
    public function setDateBirth($dateBirth)
    {
        $this->date_birth = $dateBirth;
    }

    /**
     * Get date_birth
     *
     * @return datetime 
     */
    public function getDateBirth()
    {
        return $this->date_birth;
    }
}
