<?php

namespace Nilopc\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Template,
    Sensio\Bundle\FrameworkExtraBundle\Configuration\Method,
    Symfony\Bundle\FrameworkBundle\Controller\Controller,
    Symfony\Component\Security\Core\SecurityContext,
    Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken,
    Symfony\Component\HttpKernel\Exception\NotFoundHttpException,
    JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * @Route("/user")
 */
class UserpanelController extends Controller
{


  

    /**
     * Muestra una lista de comentarios hechos por el usuario
     *
     * @Route("/comments",name="user_backend_comments")
     * @Route("/comments/{page}",name="user_backend_comments_page")     
     * @Method({"GET"}) 
     * @Template()
     */
    public function viewComments($page='')
    {

    }



}